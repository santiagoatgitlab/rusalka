import React from 'react'

import Meaning from './Meaning'
import WordDetails from './WordDetails'

function PlayCenter (props) {

    let content = ''
    if (props.status === 'GOOD'){
        content = <h3 className="cheer">Good!</h3>
    }
    else {

        let meanings = "" 
        if (typeof props.word.meaning != 'undefined'){
            let counter = 0
            let meaningsarray = Array.from(props.word.meaning)
            meanings = meaningsarray.map( meaning => {
                counter++
                return <Meaning key={counter} meaning={meaning} /> 
            })
        }

        if (props.status === 'MEANING'){

            content = meanings

        } else if (props.status === 'BAD'){

            if (props.showingDetails){
                content = 
                    <WordDetails word={props.word} meaning={meanings} baseUrl={props.baseUrl}/>
            }
            else{
                content = 
                    <div className="flex-column">
                        {meanings}
                        <p className="correction">
                            {props.word.word}
                        </p>
                        <div>
                            <button className="show-more" onClick={props.showDetails}>Ver más</button>
                        </div>
                        <input
                            type="button"
                            className="ok-button"
                            onClick={props.buttonHandler}
                            value="Ok"
                        />
                    </div>
            }
        }

    }
    return(
        <div className="play-center flex-column">
            {content}
        </div>               
    )
}

export default PlayCenter
