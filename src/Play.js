import React from 'react'

import PlayCenter from './PlayCenter'
import Stats from './Stats'

class Play extends React.Component{

    constructor(){
        super()
        this.state = {
            userResponse        : "",
            userWrongs          : 0,
            finished            : false,
            showingDetails      : true,
        }
        this.changeHandler      = this.changeHandler.bind(this)
        this.nextButtonHandler  = this.nextButtonHandler.bind(this)
        this.showDetails        = this.showDetails.bind(this)
        this.handleKeyPressed   = this.handleKeyPressed.bind(this)
    }

    componentDidUpdate(){
        this.searchInput.focus()
    }

    changeHandler(event){
        const { value } = event.target
        this.setState({ userResponse : value })
    }

    showDetails(){
        this.setState({
            showingDetails : true
        })
    }

    handleKeyPressed(event){
        if (event.which === 13){
            this.nextButtonHandler()
        }
    }

    nextButtonHandler(event){
        if (!this.state.finished){
            if (this.state.userResponse !== ""){
                let finished    = false
                let userWrongs  = this.state.userWrongs
                if (this.state.userResponse === this.props.word.word){
                    finished = true
                }
                else{
                    userWrongs++
                    if (this.state.userWrongs === this.props.settings.maxWrongs){
                        finished = true
                    }
                }
                this.setState(prevState => { 
                    return {
                        userWrongs : userWrongs,
                        finished : finished
                    }
                })
                if (finished){
                    this.updateWordStatus(userWrongs)
                }
            }
        }
        else{
            this.resetState()
            this.props.wordDone()
        }
    }

    resetState(){
        this.setState({ 
            userWrongs : 0,
            userResponse : "",
            finished : false
        })
    }

    updateWordStatus(userWrongs){
        // do call to rusov sending userWrongs
        if (userWrongs < 2){
            this.props.updateGood()
        }
        const requestOptions = {
            method : 'POST',
            headers : { 'Content-Type': 'application/json' },
            mode : 'no-cors',
            credentials : 'include',
            body : JSON.stringify({
                wrongs : userWrongs,
                wordId : this.props.word.id
            })
        }
        fetch(this.props.baseUrl+'/update_word_priority', requestOptions)
            .then(response => response )
            .then(data => {
                    console.log("response update:" + data.status)
                    if (data.status !== 0){
                        this.props.errorMsg("what?")
                    }
                })
            .catch(error=>{
                console.log(error)
                this.props.serverError();
            })
        
    }

    render(){

        const wrongMessageClasses = [
            "wrong-message", 
            this.state.userWrongs > 0 && !this.state.finished ? "show" : "hide"
        ]
        const wrongMessageText = "Please, try again"

        let status = this.state.finished && this.state.userWrongs > this.props.settings.maxWrongs
            ? 'BAD' 
            : this.state.finished && this.state.userWrongs <= this.props.settings.maxWrongs
                ? 'GOOD'
                : 'MEANING'

        const nextButtonDisabled = status === 'BAD' && !this.state.showingDetails;

        return (
            <div className="play-container">
                <PlayCenter 
                    word={this.props.word}
                    buttonHandler={this.nextButtonHandler}
                    showingDetails={this.state.showingDetails}
                    showDetails={this.showDetails}
                    baseUrl={this.props.baseUrl}
                    status={status}
                />
                <div className="controls flex-column">
                    <p className={ wrongMessageClasses.join(' ') }> 
                        {wrongMessageText}
                    </p>
                    <input 
                        className="user-response"
                        type="text"
                        name="userResponse"
                        placeholder=""
                        disabled={status!=='MEANING'}
                        onChange={this.changeHandler}
                        onKeyPress={this.handleKeyPressed}
                        value={this.state.userResponse}
                        ref={ inputEl => ( this.searchInput = inputEl ) }
                    />
                    <input
                        className="next"
                        type="button"
                        name="next"
                        disabled={nextButtonDisabled}
                        value={this.props.nextButtonValue}
                        onClick={this.nextButtonHandler}
                    />
                </div>
                <Stats 
                    stats={this.props.stats}
                />
            </div>
        )
    }
}

export default Play
