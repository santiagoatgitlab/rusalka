import React from 'react'

import Example from './Example'

class WordDetails extends React.Component{

    constructor(){
        super()
        this.state = {
            details : {}
        }
    }

    componentDidMount(){
        const requestOptions = {
            mode: 'cors',
            credentials : 'include'
        }
        fetch(this.props.baseUrl+'/get_word_details/' + this.props.word.id, requestOptions)
            .then( response => response.json() )
            .then( response => {
                console.log('setting state:',response)
                this.setState({
                    details : response
                })
            })
    }

    render(){
        if (typeof this.state.details.examples !== 'undefined'){
            console.log(this.state.details)
            let counter = 0
            const examples = this.state.details.examples.map( example => {
                counter++
                return <Example key={counter} example={example} /> 
            })
            const info      = "Adjective - Plural - Masculine"
            return(


                <div className="word-details">
                    <h3>{this.props.word.word}</h3>
                    <div className="meaning">{this.props.meaning}</div>
                    <div className="info">{info}</div>
                    <div className="examples">
                        <b>examples</b>
                        {examples}
                    </div>
                </div>
            )
        }
        return null
    }
}

export default WordDetails
