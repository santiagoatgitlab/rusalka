import React from 'react'

function Meaning (props) {
    return (
        <p>
            {props.meaning}
        </p>
    )
}

export default Meaning
