import React from 'react'

import PlayContainer from './PlayContainer.js'
import FromRussian from './FromRussian.js'

function Menu (props){
    return (
        <div className="menu">
            <button
                onChange={props.buttonHandler}
                name="toRussian"
            >Russian words</button>
            <button
                onChange={props.buttonHandler}
                name="fromRussian"    
            >Meanings</button>
        </div>
    )
}

function Content (props){
    if (props.mode === ""){
        return(
            <div>
                <Menu buttonHandler={props.buttonHandler} />
            </div>
        )
    }
    else if (props.mode === "toRussian"){
        return(
            <div>
                <PlayContainer />
            </div>
        )
    }
    else if (props.mode === "fromRussian"){
        return(
            <div>
                <FromRussian />
            </div>
        )
    }
}

export default Menu
