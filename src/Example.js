import React from 'react'

function Example (props){
    return (
        <p>
            {props.example} 
        </p>
    )
}

export default Example
