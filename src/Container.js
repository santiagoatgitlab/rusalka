import React, {useState} from 'react'
    
import Menu from './Menu'

function Container(){

    const [ content, setContent ] = useState("menu")

    if (content == "menu"){
        return (
            <div className="container">
                <Menu />
            </div>
        )
    }

}


export default Container
