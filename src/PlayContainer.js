import React from 'react'

import Play from './Play'

class PlayContainer extends React.Component {

    settings = {
        maxWrongs : 1
    }

    constructor(){
        super()
        this.state = {
            words : {},
            currentWordIndex : 0,
            serverProblems : false,
            errorMsg : "",
            stats               : {
                total : 0,
                sessionTotal : 0,
                sessionGood : 0
            }
        }
        this.wordDone           = this.wordDone.bind(this)
        this.updateGood         = this.updateGood.bind(this)
        this.tryAgain           = this.tryAgain.bind(this)
        this.serverError        = this.serverError.bind(this)
        this.errorMsg           = this.errorMsg.bind(this)
    }

    loadWords(){
        const requestOptions = {
            mode: 'cors',
            credentials : 'include'
        }
        fetch(this.props.baseUrl+'/get_next_words', requestOptions)
            .then( async response => { return response.json() } )
            .then( response => {
                console.log('words: ',response);
                this.setState({ 
                    words : response.words,
                    stats : response.stats,
                    currentWordIndex : 0
                })
            })
    }

    componentDidMount(){
        this.loadWords()
    }

    wordDone(){
        this.setState(  function (prevState) { 
            return { 
                currentWordIndex : prevState.currentWordIndex + 1,
                stats : {
                    ...prevState.stats,
                    sessionTotal : prevState.stats.sessionTotal + 1
                }
            }
        })
    }

    updateGood(){
        this.setState(  function (prevState) { 
            return { 
                stats : {
                    ...prevState.stats,
                    sessionGood : prevState.stats.sessionGood + 1
                }
            }
        })
    }

    tryAgain(){
        console.log('trying to try');
        this.setState({
            serverProblems:false,
            errorMsg: ""
        })
    }

    serverError(){
        this.setState({serverProblems:true})
    }

    errorMsg(msg){
        this.setState({errorMsg:msg})
    }

    render(){
        let currentWord = {}
        if (this.state.words.length > 0){
            if (this.state.currentWordIndex === this.state.words.length){
                this.loadWords()
            }
            else{
                currentWord = this.state.words[ this.state.currentWordIndex ]
            }
        }
        if (this.state.serverProblems){
            return(
                <div className="server-problems">
                    <p>
                        There are some connection problems.
                    </p>
                    <p>
                        please check your internet connection and try again
                    </p>
                    <button onClick={this.tryAgain}>Try again</button>
                </div>
            )
        } else if (this.state.errorMsg !== ""){
            return(
                <div className="error-msg">
                    <p>
                        An error occurred in the server
                    </p>
                    <p>
                        Hi!
                        {this.state.errorMsg}
                    </p>
                    <button onClick={this.tryAgain}>Try again</button>
                </div>
            )
        }
        return (
            <main>
                <Play
                    word={ currentWord }
                    status={this.status}
                    userResponse={this.state.currentUserResponse}
                    nextButtonValue="Next"
                    settings={this.settings}
                    wordDone={this.wordDone}
                    updateGood={this.updateGood}
                    serverError={this.serverError}
                    errorMsg={this.errorMsg}
                    baseUrl={this.props.baseUrl}
                    stats={this.state.stats}
                />
            </main>
        )
    }
}

export default PlayContainer
