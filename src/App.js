import React from 'react'

import Header from './Header.js'
import PlayContainer from './PlayContainer.js'
import Footer from './Footer.js'

function App () {

    const baseUrl = 'http://rusov.catpurple.net/'

    return (
        <div className="app flex-column">
            <Header />
            <PlayContainer
                baseUrl={baseUrl}
             />
            <Footer />
        </div>
    )
}

export default App
