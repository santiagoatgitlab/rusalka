import React from 'react'

function Stats (props){
    return(
        <div className="stats">
            <div className="total">{props.stats.total}</div>
            <div className="session-total">{props.stats.sessionTotal}</div>
            <div className="session-learn">{props.stats.sessionGood}</div>
        </div>
    )
}

export default Stats
